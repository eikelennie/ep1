#ifndef CLIENTE_HPP
#define CLIENTE_HPP
#include <iostream> 
#include <string> 

using namespace std; 

class Cliente {
    protected: 
    string nome;
    string cpf;
    string email;
   
    
    public: 
    Cliente ();
    Cliente(string nome, string cpf, string email);
    ~Cliente();

    void setNome(string nome);
    string getNome();
    void setCpf(string cpf);
    string getCpf();
    void setEmail(string email);
    string getEmail();
    

    void mostrarDados();
}; 
#endif
