#ifndef SOCIO_HPP
#define SOCIO_HPP
#include "cliente.hpp"
#include <string> 

class Socio : public Cliente {
    protected:
        string dataDeRegistro; 
    public: 
        Socio(); 
        Socio(string nome, string cpf, string email, string dataDeRegistro);
        Socio(string dataDeRegistro);
        ~Socio(); 

        void setData(string dataDeRegistro);
        string getData();
        


};
#endif
