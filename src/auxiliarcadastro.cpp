#include <iostream>
#include <string> 
#include <vector>
#include "auxiliarcadastro.hpp"

using namespace std; 
template <typename var>
var leitura (){
    while(true){
    var lido;
    cin >> lido; 
    if(cin.fail()){
        cin.clear();
        cin.ignore(32776, '\n');
        cout << "Entrada Inválida\n"; 
    }
    else 
    return lido; 
    }
}
string Str(){
    string texto;
    cin.ignore();
     getline(cin,texto);
      
    return texto; 
}

Item CadastrarItem(){
     system("clear");
    cout << "Registrando novo item" << endl << endl; 
    cout << "Nome do item: "; 
    string nome = Str();
    cout << "Quantidade no estoque: "; 
    long int qtd = leitura<long int>(); 
    cout << "Valor: "; 
    float valor = leitura<float>();
    cout << "Informe o número de categorias desse item: "; 
   int a = 0;
    while(a <=0){
        a = leitura <int> (); 
        if(a == 1)
            cout << "Informe sua categoria" << endl; 
        else if(a >1) 
            cout << "Informe suas categorias" << endl; 
    }
    vector <string> categoria; 
    for (int i=0; i<a ; i++){
        cout << i+1 <<"º categoria" << endl; 
        categoria.push_back(Str()); 
    }
    Item item;
    system("clear");
    return item = Item (nome, qtd, valor, categoria);
}
