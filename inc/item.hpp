#ifndef ITEM_HPP
#define ITEM_HPP

#include <iostream>
#include <string>
#include <vector>

using namespace std;

 class Item {
     private: 
     string nitem;
     long int qtd;
     float valor;
     vector <string> categoria; 
     
    public: 
     Item();
     Item(string nitem, long int qtd, float valor, vector <string> categoria);
     ~Item();

     string getItem();
     void setItem(string nitem);

     long int getQtd();
     void setQtd(long int qtd);

     float getValor();
     void setValor(float valor);

     void mostrarDados();
     
 };
#endif