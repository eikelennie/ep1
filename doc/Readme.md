########################################Instruções##############################

1- Para executar o código é necessário um compilador C++ (Visual Studio Code).

2- Será necessário as seguintes bibliotecas no computador:

*Unistd.h

3-Como usar:

*1-Você irá executar o programa na linha de comando escrevendo: "make run".

*2- Acessar o modo venda digitando '1' ou modo estoque digitando '2' e '0' para encerrar.

*3- Se acessar o modo venda, digite '2' para cadastrar cliente e '3' para sócio e '0' para encerrar.

*4- Se acessar o modo estoque, digite '1' para adicionar item e '2' para listar item e '0' para encerrar.